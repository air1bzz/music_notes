/*
Package musicnote provides methods to deal, compare and calculate
frequencies of musical notes.
*/
package musicnote

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
)

/*
MusicNote represents a musical note. Octave and letter are setted as defined in
the 'Scientific Pitch Notation'.
*/
type MusicNote struct {
	letter sym
	octave int
}

type sym int

const (
	c sym = iota + 1
	cs
	d
	ds
	e
	f
	fs
	g
	gs
	a
	as
	b
)

var regLetter = regexp.MustCompile(`^((?i:a|a#|b|c|c#|d|d#|e|f|f#|g|g#))(-?\d+)$`)
var regLatin = regexp.MustCompile(`^((?i:la|la#|si|do|do#|re|re#|mi|fa|fa#|sol|sol#))(-?\d+)$`)

/*
NewMidiNote creates an instance of a 'MusicNote' struct from a given string
which is a valid MIDI note notation.
*/
func NewMidiNote(note string) (MusicNote, error) {
	n, err := newNote(note, regLetter, 1, midi)
	if err != nil {
		return n, err
	}
	if n.octave < 1 {
		return n, fmt.Errorf("Octave can't be less than 0 but is %d", n.octave-1)
	}

	return n, nil
}

/*
NewLatinNote creates an instance of a 'MusicNote' struct from a given string
which is a valid latin note notation.
*/
func NewLatinNote(note string) (MusicNote, error) {
	n, err := newNote(note, regLatin, 1, latin)
	if err != nil {
		return n, err
	}

	return n, nil
}

/*
New creates an instance of a 'MusicNote' struct from a given string
which is a valid 'Scientific Pitch Notation' note.
*/
func New(note string) (MusicNote, error) {
	n, err := newNote(note, regLetter, 0, spn)
	if err != nil {
		return n, err
	}

	return n, nil
}

// Next gives the next note of a given note.
func (m MusicNote) Next() MusicNote {
	if m.letter == b {
		m.letter = c
		m.octave++
	} else {
		m.letter++
	}

	return m
}

// Prev gives the previous note of a given note.
func (m MusicNote) Prev() MusicNote {
	if m.letter == c {
		m.letter = b
		m.octave--
	} else {
		m.letter--
	}

	return m
}

// Transpose gives a note according to a given interval of semi-tones.
// The interval can be positive or negative.
func (m MusicNote) Transpose(semiTones int) MusicNote {
	if semiTones > 1 {
		for i := 0; i < semiTones; i++ {
			m = m.Next()
		}
	} else {
		for i := 0; i > semiTones; i-- {
			m = m.Prev()
		}
	}

	return m
}

// Frequency returns the frequency of a music note in Hertz.
func (m MusicNote) Frequency(diap float64, harmonic int) float64 {
	if harmonic < 1 {
		harmonic = 1
	}
	index := m.letter - 1
	pow := math.Pow(2, float64(m.octave)+((float64(index)-9.0)/12.0))

	return ((diap / 4) * pow) / 4 * float64(harmonic)
}

// Compare compares two music notes each other, returning -1 if receiver is
// lower than argument, 0 if equal or 1 if higher. From lower for a low-pitched
// note to higher for a high-pitched note.
func Compare(m MusicNote, other MusicNote) int {
	if (m.letter < other.letter &&
		m.octave <= other.octave) ||
		m.octave < other.octave {
		return -1
	} else if m == other {
		return 0
	}

	return 1
}

/*
String returns a music note representation in 'Scientific Pitch Notation'. It
also satisfies 'fmt.Stringer' interface.
*/
func (m MusicNote) String() string {
	return fmt.Sprintf("%v%d", strNotes[spn][m.letter-1], m.octave)
}

/*
LatinString returns a music note representation in 'Latin Pitch Notation'.
Namely: do, re, mi, fa, sol, la, si...
*/
func (m MusicNote) LatinString() string {
	return fmt.Sprintf("%v %d", strNotes[latin][m.letter-1], m.octave-1)
}

/*
MidiString returns a music note representation in 'Midi Pitch Notation'. Same
as SPN but one octave lower.
*/
func (m MusicNote) MidiString() string {
	return fmt.Sprintf("%v%d", strNotes[midi][m.letter-1], m.octave-1)
}

/*
MidiKey returns the key number in MIDI protocol.
MIDI notes are numbered from 0 to 127 assigned to C-1 to G9.
*/
func (m MusicNote) MidiKey() (uint8, error) {
	if Compare(m, MusicNote{c, -1}) == -1 || Compare(m, MusicNote{g, 9}) == 1 {
		return 0, fmt.Errorf("MIDI notes must be beetween C-1 and G9, got %v", m)
	}
	return uint8((m.octave+1)*12 + int(m.letter) - 1), nil
}

// Range can easily make ranges of music notes.
func Range(from MusicNote, to MusicNote) []MusicNote {
	var notes []MusicNote
	for ; Compare(from, to) != 1; from = from.Next() {
		notes = append(notes, from)
	}

	return notes
}

/*
MusicNotes implements sort.Interface for []Musicnote.
It uses Compare function.
*/
type MusicNotes []MusicNote

func (m MusicNotes) Len() int           { return len(m) }
func (m MusicNotes) Swap(i, j int)      { m[i], m[j] = m[j], m[i] }
func (m MusicNotes) Less(i, j int) bool { return Compare(m[i], m[j]) < 0 }

func getIndexOf(note string, m mode) int {
	for j, n := range strNotes[m] {
		if strings.ToLower(n) == strings.ToLower(note) {
			return j
		}
	}

	return -1
}

func newNote(note string, reg *regexp.Regexp, octShift int, m mode) (MusicNote, error) {
	obj := new(MusicNote)
	if !reg.MatchString(note) {
		return *obj, fmt.Errorf("'%v' is not a music note", note)
	}

	matches := reg.FindStringSubmatch(note)

	letter := strings.ToLower(matches[1])
	obj.letter = sym(getIndexOf(letter, m) + 1)
	octave, _ := strconv.Atoi(matches[2])
	obj.octave = octave + octShift

	return *obj, nil
}
