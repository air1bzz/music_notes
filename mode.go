package musicnote

type mode int

const (
	latin mode = iota + 1
	spn
	midi
)

var letterNotes = []string{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"}
var latinNotes = []string{"do", "do#", "re", "re#", "mi", "fa", "fa#", "sol", "sol#", "la", "la#", "si"}

var strNotes = map[mode][]string{
	latin: latinNotes,
	spn:   letterNotes,
	midi:  letterNotes,
}
