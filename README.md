# Music Notes

This package aims to work with music notes.

**Features:**

- create and display music notes in multiple formats (latin, midi, scientific)
- get frequency switch harmonics
- compare, transpose, walk
- create ranges

## Install

`go get gitlab.com/air1bzz/music_notes`

## Usage

```go
package main

import (
    "fmt"
    "sort"

    musicnote "gitlab.com/air1bzz/music_notes"
)

func main() {
    m, _ := musicnote.New("A4")
    fmt.Println(m.LatinString())

    fmt.Println(m.Frequency(440, 1))
    fmt.Println(m.Frequency(440, 3))

    m2, _ := musicnote.NewLatinNote("do3")
    k, _ := m2.MidiKey()
    fmt.Printf("%v => %d\n", m2.MidiString(), k)

    fmt.Println(musicnote.Compare(m, m2))
    fmt.Println(musicnote.Compare(m, m))
    fmt.Println(musicnote.Compare(m2, m))

    fmt.Println(musicnote.Range(m2.Prev(), m.Next()))

    m3, _ := musicnote.New("c#4")
    list := musicnote.MusicNotes([]musicnote.MusicNote{m, m2, m3})
    fmt.Println(list)
    sort.Sort(list)
    fmt.Println(list)
}

// Output:
// la 3
// 440
// 1320
// C3 => 60
// 1
// 0
// -1
// [B3 C4 C#4 D4 D#4 E4 F4 F#4 G4 G#4 A4 A#4]
// [A4 C4 C#4]
// [C4 C#4 A4]
```
