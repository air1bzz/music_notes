package musicnote

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

func TestNewMidiNote(t *testing.T) {
	sample1 := "si#10"

	cases := []struct {
		name   string
		params string
		want   interface{}
	}{
		{
			name:   "Success",
			params: "a#4",
			want:   MusicNote{as, 5},
		},
		{
			name:   "Fails when octave is less than 0",
			params: "a-1",
			want:   errors.New("Octave can't be less than 0 but is -1"),
		},
		{
			name:   "Fails when param is not a valid note",
			params: sample1,
			want:   fmt.Errorf("'%v' is not a music note", sample1),
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got, err := NewMidiNote(c.params)
			if err != nil {
				if !reflect.DeepEqual(err, c.want) {
					t.Errorf(
						"NewMidiNote(%q) == ERROR\ngot : %v\nwant: %v",
						c.params, err, c.want,
					)
				}
			} else {
				if !reflect.DeepEqual(got, c.want) {
					t.Errorf("NewMidiNote(%q) == %v, want %v", c.params, got, c.want)
				}
			}
		})
	}
}

func TestNewLatinNote(t *testing.T) {
	sample1 := "B10"

	cases := []struct {
		name   string
		params string
		want   interface{}
	}{
		{
			name:   "Success",
			params: "la#4",
			want:   MusicNote{as, 5},
		},
		{
			name:   "Fails when param is not a valid note",
			params: sample1,
			want:   fmt.Errorf("'%v' is not a music note", sample1),
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got, err := NewLatinNote(c.params)
			if err != nil {
				if !reflect.DeepEqual(err, c.want) {
					t.Errorf("NewLatinNote(%q) == ERROR: %v, want %v", c.params, err, c.want)
				}
			} else {
				if !reflect.DeepEqual(got, c.want) {
					t.Errorf("NewLatinNote(%q) == %v, want %v", c.params, got, c.want)
				}
			}
		})
	}
}

func TestNew(t *testing.T) {
	sample1 := "la10"

	cases := []struct {
		name   string
		params string
		want   interface{}
	}{
		{
			name:   "Success",
			params: "a#4",
			want:   MusicNote{as, 4},
		},
		{
			name:   "Fails when param is not a valid note",
			params: sample1,
			want:   fmt.Errorf("'%v' is not a music note", sample1),
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got, err := New(c.params)
			if err != nil {
				if !reflect.DeepEqual(err, c.want) {
					t.Errorf("New(%q) == ERROR: %v, want %v", c.params, err, c.want)
				}
			} else {
				if !reflect.DeepEqual(got, c.want) {
					t.Errorf("New(%q) == %v, want %v", c.params, got, c.want)
				}
			}
		})
	}
}

func TestNext(t *testing.T) {
	cases := []struct {
		name   string
		params MusicNote
		want   MusicNote
	}{
		{
			name:   "Switch from B to C and increase octave",
			params: MusicNote{b, 2},
			want:   MusicNote{c, 3},
		},
		{
			name:   "Switch from E to F",
			params: MusicNote{e, 10},
			want:   MusicNote{f, 10},
		},
		{
			name:   "Switch to sharp note",
			params: MusicNote{f, 4},
			want:   MusicNote{fs, 4},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.params.Next()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.Next() == %v, want %v", c.params, got, c.want)
			}
		})
	}
}

func TestPrev(t *testing.T) {
	cases := []struct {
		name   string
		params MusicNote
		want   MusicNote
	}{
		{
			name:   "Switch from C to B and decrease octave",
			params: MusicNote{c, 2},
			want:   MusicNote{b, 1},
		},
		{
			name:   "Switch from F to E",
			params: MusicNote{f, 10},
			want:   MusicNote{e, 10},
		},
		{
			name:   "Switch to natural note",
			params: MusicNote{fs, 4},
			want:   MusicNote{f, 4},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.params.Prev()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.Next() == %v, want %v", c.params, got, c.want)
			}
		})
	}
}

func TestTranspose(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNote
		params   int
		want     MusicNote
	}{
		{"Returns the same when 0", MusicNote{c, 2}, 0, MusicNote{c, 2}},
		{"Returns sharp notes", MusicNote{c, 2}, 3, MusicNote{ds, 2}},
		{"Returns octave", MusicNote{f, 10}, 12, MusicNote{f, 11}},
		{"Returns with negative param", MusicNote{fs, 4}, -2, MusicNote{e, 4}},
		{"Returns with negative octave", MusicNote{fs, 4}, -24, MusicNote{fs, 2}},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.Transpose(c.params)
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.AddSemiTones(%d) == %v, want %v", c.receiver, c.params, got, c.want)
			}
		})
	}
}

func ExampleMusicNote_Transpose() {
	do1, _ := NewLatinNote("do1")
	// Get octave
	fmt.Println(do1.Transpose(12).LatinString())
	// Get suboctave
	fmt.Println(do1.Transpose(-12).LatinString())
	// Output:
	// do 2
	// do 0
}

func TestGetIndexof(t *testing.T) {
	type params struct {
		note string
		mode mode
	}

	cases := []struct {
		name string
		params
		want int
	}{
		{
			name: "Success",
			params: params{
				note: "A",
				mode: spn,
			},
			want: 9,
		},
		{
			name: "Success",
			params: params{
				note: "V",
				mode: latin,
			},
			want: -1,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := getIndexOf(c.note, c.mode)
			if got != c.want {
				t.Errorf("getIndexOf(%#v) == %d, want %d", c.params, got, c.want)
			}
		})
	}
}

func TestFrequency(t *testing.T) {
	type params struct {
		diapason float64
		harmonic int
	}

	cases := []struct {
		name     string
		receiver MusicNote
		params
		want interface{}
	}{
		{
			name:     "Fundamental A4",
			receiver: MusicNote{a, 4},
			params: params{
				diapason: 440,
				harmonic: 1,
			},
			want: 440.0,
		},
		{
			name:     "Fundamental A3",
			receiver: MusicNote{a, 3},
			params: params{
				diapason: 440,
				harmonic: 1,
			},
			want: 220.0,
		},
		{
			name:     "Fundamental A2",
			receiver: MusicNote{a, 2},
			params: params{
				diapason: 440,
				harmonic: 1,
			},
			want: 110.0,
		},
		{
			name:     "Fundamental A2 / Diapason 435",
			receiver: MusicNote{a, 4},
			params: params{
				diapason: 435,
				harmonic: 1,
			},
			want: 435.0,
		},
		{
			name:     "Fundamental A2 / Harmonic 2",
			receiver: MusicNote{a, 4},
			params: params{
				diapason: 440,
				harmonic: 2,
			},
			want: 880.0,
		},
		{
			name:     "Fundamental A0 / Harmonic 16",
			receiver: MusicNote{a, 0},
			params: params{
				diapason: 440,
				harmonic: 16,
			},
			want: 440.0,
		},
		{
			name:     "Fundamental A3 / Harmonic 3",
			receiver: MusicNote{a, 3},
			params: params{
				diapason: 440,
				harmonic: 3,
			},
			want: 660.0,
		},
		{
			name:     "Fundamental A4 / Harmonic 0",
			receiver: MusicNote{a, 4},
			params: params{
				diapason: 440,
				harmonic: 0,
			},
			want: 440.0,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.Frequency(c.diapason, c.harmonic)
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.Frequency(%f, %d) == %f, want %f", c.receiver, c.diapason, c.harmonic, got, c.want)
			}
		})
	}
}

func ExampleMusicNote_Frequency() {
	a4, _ := New("a4")
	fmt.Println(a4.Frequency(440, 1))
	fmt.Println(a4.Transpose(-12).Frequency(435, 2))
	fmt.Println(a4.Transpose(-24).Frequency(440, 3))
	// Output:
	// 440
	// 435
	// 330
}

func TestCompare(t *testing.T) {
	type params struct {
		note1 MusicNote
		note2 MusicNote
	}

	cases := []struct {
		name string
		params
		want int
	}{
		{
			name:   "C3 is less than A3",
			params: params{MusicNote{c, 3}, MusicNote{a, 3}},
			want:   -1,
		},
		{
			name:   "B3 is less than C4",
			params: params{MusicNote{b, 3}, MusicNote{c, 4}},
			want:   -1,
		},
		{
			name:   "C2 is less than C3",
			params: params{MusicNote{c, 2}, MusicNote{c, 3}},
			want:   -1,
		},
		{
			name:   "C2 is equal to C2",
			params: params{MusicNote{c, 2}, MusicNote{c, 2}},
			want:   0,
		},
		{
			name:   "C3 is greater than B2",
			params: params{MusicNote{c, 3}, MusicNote{b, 2}},
			want:   1,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := Compare(c.note1, c.note2)
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("Compare(%v, %v)\ngot :%v\nwant: %v", c.note1, c.note2, got, c.want)
			}
		})
	}
}

func TestRange(t *testing.T) {
	type params struct {
		from MusicNote
		to   MusicNote
	}

	cases := []struct {
		name string
		params
		want []MusicNote
	}{
		{
			name:   "Make slice of notes",
			params: params{MusicNote{c, 3}, MusicNote{e, 3}},
			want: []MusicNote{
				{c, 3},
				{cs, 3},
				{d, 3},
				{ds, 3},
				{e, 3},
			},
		},
		{
			name:   "Make slice of uniq note",
			params: params{MusicNote{c, 3}, MusicNote{c, 3}},
			want:   []MusicNote{{c, 3}},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := Range(c.from, c.to)
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("Range(%v, %v) == %v, want %v", c.from, c.to, got, c.want)
			}
		})
	}
}

func TestString(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNote
		want     string
	}{
		{
			name:     "Returns string representation",
			receiver: MusicNote{a, 4},
			want:     "A4",
		},
		{
			name:     "Returns string representation with sharp",
			receiver: MusicNote{as, 4},
			want:     "A#4",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.String()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.String() == %v, want %v", c.receiver, got, c.want)
			}
		})
	}
}

func TestLatinString(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNote
		want     string
	}{
		{
			name:     "Returns string representation",
			receiver: MusicNote{a, 4},
			want:     "la 3",
		},
		{
			name:     "Returns string representation with sharp",
			receiver: MusicNote{as, 4},
			want:     "la# 3",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.LatinString()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.LatinString() == %v, want %v", c.receiver, got, c.want)
			}
		})
	}
}

func TestMidiString(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNote
		want     string
	}{
		{
			name:     "Returns string representation",
			receiver: MusicNote{a, 4},
			want:     "A3",
		},
		{
			name:     "Returns string representation with sharp",
			receiver: MusicNote{as, 4},
			want:     "A#3",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.MidiString()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.MidiString() == %v, want %v", c.receiver, got, c.want)
			}
		})
	}
}

func TestMidiKey(t *testing.T) {
	aboveMax := MusicNote{gs, 9}
	belowMin := MusicNote{b, -2}

	cases := []struct {
		name     string
		receiver MusicNote
		want     interface{}
	}{
		{
			"Success A4",
			MusicNote{a, 4},
			uint8(69),
		},
		{
			"Success C-1 (min)",
			MusicNote{c, -1},
			uint8(0),
		},
		{
			"Success G9 (max)",
			MusicNote{g, 9},
			uint8(127),
		},
		{
			"Fails when above max",
			aboveMax,
			fmt.Errorf("MIDI notes must be beetween C-1 and G9, got %v", aboveMax),
		},
		{
			"Fails when below min",
			belowMin,
			fmt.Errorf("MIDI notes must be beetween C-1 and G9, got %v", belowMin),
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got, err := c.receiver.MidiKey()
			if err != nil {
				if !reflect.DeepEqual(err, c.want) {
					t.Errorf("%v.Midikey() == ERROR\ngot : %v\nwant: %v", c.receiver, err, c.want)
				}
			} else {
				if !reflect.DeepEqual(got, c.want) {
					t.Errorf("%v.Midikey() == %v, want %v", c.receiver, got, c.want)
				}
			}
		})
	}
}

func TestLen(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNotes
		want     int
	}{
		{
			"Success",
			MusicNotes{MusicNote{a, 3}, MusicNote{a, 4}, MusicNote{a, 5}},
			3,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.Len()
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.Len()\ngot : %v\nwant: %v", c.receiver, got, c.want)
			}
		})
	}
}

func TestSwap(t *testing.T) {
	receiver := MusicNotes{MusicNote{a, 3}, MusicNote{a, 4}}

	cases := []struct {
		name     string
		receiver MusicNotes
		want     MusicNotes
	}{
		{
			"Success",
			receiver,
			MusicNotes{MusicNote{a, 4}, MusicNote{a, 3}},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			c.receiver.Swap(0, 1)
			if !reflect.DeepEqual(c.receiver, c.want) {
				t.Errorf("%v.Swap()\nwant: %v", c.receiver, c.want)
			}
		})
	}
}

func TestLess(t *testing.T) {
	cases := []struct {
		name     string
		receiver MusicNotes
		want     bool
	}{
		{
			"Success",
			MusicNotes{MusicNote{a, 3}, MusicNote{a, 4}},
			true,
		},
		{
			"Success",
			MusicNotes{MusicNote{a, 4}, MusicNote{a, 3}},
			false,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := c.receiver.Less(0, 1)
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("%v.Swap()\ngot : %v\nwant: %v", c.receiver, got, c.want)
			}
		})
	}
}
